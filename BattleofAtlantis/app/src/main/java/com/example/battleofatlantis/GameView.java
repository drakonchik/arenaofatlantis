package com.example.battleofatlantis;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.SurfaceView;

public class GameView extends SurfaceView implements Runnable
{
    private Thread thread;
    private boolean isPlaying;
    private int screenX, screenY;
    static float screenRatioX, screenRatioY;
    private Paint paint;
    private Background background;
    private Enemy Alvis;


    public GameView(Context context, int screenX, int screenY)
    {
        super(context);

        this.screenX = screenX;
        this.screenY = screenY;

        screenRatioY = 1920f / screenY;
        screenRatioX = 1080f / screenX;

        background = new Background(screenX,screenY, getResources());
        Alvis = new Enemy(screenX,screenY,getResources());


    }

    @Override
    public void run()
    {
        while (isPlaying)
        {
            Update();
            Draw();
            Sleep();
        }
    }

    private void Update()
    {



    }

    private void Draw()
    {
        if(getHolder().getSurface().isValid())
        {
            Canvas canvas = getHolder().lockCanvas();

            canvas.drawBitmap(background.background, background.x, background.y, paint);
            canvas.drawBitmap(Alvis.AlvisEnemy, Alvis.x, Alvis.y, paint);


            getHolder().unlockCanvasAndPost(canvas);
        }
    }

    private void Sleep()
    {
        try
        {
            thread.sleep(17);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public void resume()
    {
        isPlaying = true;
        thread = new Thread(this);
        thread.start();
    }

    public void pause()
    {
        try
        {
            isPlaying = false;
            thread.join();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }
}
